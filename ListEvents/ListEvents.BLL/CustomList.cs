﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListEvents.BLL
{
    public class CustomList<T> : Collection<T>
    {
        public event EventHandler<ListEventArgs<T>> BeforeAdded;
        public event EventHandler<ListEventArgs<T>> AfterAdded;
        public event EventHandler<ListEventArgs<T>> BeforeDeleted;
        public event EventHandler<ListEventArgs<T>> AfterDeleted;
        public event EventHandler<ListEventArgs<T>> Sorted;
        private void OnBeforeAdded(ListEventArgs<T> e) { BeforeAdded?.Invoke(this, e); }
        private void OnAfterAdded(ListEventArgs<T> e) { AfterAdded?.Invoke(this, e); }
        private void OnBeforeDeleted(ListEventArgs<T> e) { BeforeDeleted?.Invoke(this, e); }
        private void OnAfterDeleted(ListEventArgs<T> e) { AfterDeleted?.Invoke(this, e); }
        private void OnSorted(ListEventArgs<T> e) { Sorted?.Invoke(this, e); }

        protected override void InsertItem(int index, T item)
        {
            var e = new ListEventArgs<T>($"The item '{item}' will be added at index {index}.", item);
            OnBeforeAdded(e);

            if (e.IsValid)
            {
                base.InsertItem(index, item);
                OnAfterAdded(new ListEventArgs<T>($"The item '{item}' was successfully added to the list.\n", item));
            }
            else
            {
                OnAfterAdded(new ListEventArgs<T>($"Fuck you.\n", item));
            }          
        }

        protected override void RemoveItem(int index)
        {
            OnBeforeDeleted(new ListEventArgs<T>($"The item will be removed at index {index}."));
            base.RemoveItem(index);
            OnAfterDeleted(new ListEventArgs<T>($"The item was successfully removed from the list.\n"));
        }

        public void Sort()
        {
            ((List<T>)Items).Sort();
            OnSorted(new ListEventArgs<T>($"The list was successfully sorted.\n"));
        }
    }
}
