﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListEvents.BLL
{
    public class ListEventArgs<T>
    {
        public string Message { get; set; }
        public T Value { get; }
        public bool IsValid { get; set; }

        public ListEventArgs(string msg, T value)
        {
            Message = msg;
            Value = value;
            IsValid = true;
        }
        public ListEventArgs(string msg)
        {
            Message = msg;
            IsValid = true;
        }
    }
}
