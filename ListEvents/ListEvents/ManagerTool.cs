﻿using ListEvents.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListEvents
{
    enum Action
    {
        Add = 1,
        Insert,
        Remove,
        Sort,
        Print,
        Quit
    }

    public class ManagerTool
    {
        private CustomList<int> _myList;

        public ManagerTool()
        {
            _myList = new CustomList<int>();
            _myList.BeforeAdded += AddRuleHandler;
            _myList.BeforeAdded += MessageHandler;
            _myList.AfterAdded += MessageHandler;
            _myList.BeforeDeleted += MessageHandler;
            _myList.AfterDeleted += MessageHandler;
            _myList.Sorted += MessageHandler;
        }

        private void AddRuleHandler(object sender, ListEventArgs<int> e)
        {
            e.IsValid = e.Value % 2 == 0;
        }

        public void Start()
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("What would you like to do?: \n1. Add item \t4. Sort list \n2. Insert item \t5. Print list \n3. Remove item \t6. Quit \n");
                Console.ResetColor();
                Action action = 0;
                try
                {
                    action = (Action)int.Parse(Console.ReadLine());
                    switch (action)
                    {
                        case Action.Add:
                            AddItem();
                            break;
                        case Action.Insert:
                            InsertItem();
                            break;
                        case Action.Remove:
                            RemoveItem();
                            break;
                        case Action.Sort:
                            _myList.Sort();
                            break;
                        case Action.Print:
                            PrintList();
                            break;
                        case Action.Quit:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Invalid operation.\n");
                            break;
                    }
                }
                catch (FormatException) { Console.WriteLine("Invalid operation.\n"); }
                catch (ArgumentOutOfRangeException) { Console.WriteLine("Invalid index.\n"); }
                catch (OverflowException) { Console.WriteLine($"Invalid value. Use values between 0 and {int.MaxValue} only.\n"); }
                catch (InvalidOperationException) { Console.WriteLine("No such item exists in the list.\n"); }
            }
        }

        public void AddItem()
        {
            Console.Write("\nEnter value of the item you'd like to add: ");
            int value = int.Parse(Console.ReadLine());
            //if (value < 0)
            //    Console.WriteLine("Negative values are not allowed.\n");
            //else
                _myList.Add(value);
        }

        public void RemoveItem()
        {
            Console.Write("\nEnter value of the item you'd like to remove: ");
            int value = int.Parse(Console.ReadLine());
            if (!_myList.Remove(value))
                throw new InvalidOperationException();
        }

        public void InsertItem()
        {
            Console.Write("\nEnter value of the item you'd like to add: ");
            int value = int.Parse(Console.ReadLine());
            Console.Write("Enter index you'd like that item to be at: ");
            int index = int.Parse(Console.ReadLine());
            if (value < 0)
                Console.WriteLine("Negative values are not allowed.\n");
            else
                _myList.Insert(index, value);
        }

        public void PrintList()
        {
            Console.WriteLine("\nYour list:");
            Console.Write("[ ");
            foreach (int i in _myList)
                Console.Write(i + " ");
            Console.Write("]\n\n");
            Console.ReadKey();
        }

        static void MessageHandler(object sender, ListEventArgs<int> e)
        {
            Console.WriteLine(e.Message);
            Console.ReadKey();
        }
    }
}
