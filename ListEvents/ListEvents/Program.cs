﻿using ListEvents.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new ManagerTool();
            manager.Start();
        }
    }
}