﻿using System;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleLife
{
    class Program
    {
        static void Main(string[] args)
        {
            var Life = new Life(24, 77);
            Life.GenerateWorld();
            Console.Write("Добро пожаловать в жизнь\nНажмите любую клавишу, чтобы начать\n\nСмотри код, можно убрать периодическую генерацию фигур и \nгенерировать новое поколение нажатием клавиши");
            Console.ReadKey();
            Life.Draw();
            while (true)
            {
                Life.SaveWorld();
                Life.CalculateNextGen();
                Life.LoadWorld();
                Life.Draw();
                Thread.Sleep(30);
            }
        }
    }
}
