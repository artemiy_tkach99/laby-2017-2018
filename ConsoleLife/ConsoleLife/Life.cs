﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleLife
{
    public class Life
    {
        private int Height { get; }
        private int Width { get; }
        private bool[,] WorldMap;
        private bool[,] NextGen;

        public Life(int heigth, int width)
        {
            Height = heigth;
            Width = width;
            WorldMap = new bool[Height, Width];
            NextGen = new bool[Height, Width];
        }

        public void GenerateWorld()
        {
            Random rnd = new Random();
            for (int i = 0; i < WorldMap.GetLength(0); i++)
                for (int j = 0; j < WorldMap.GetLength(1); j++)
                    WorldMap[i, j] = rnd.Next(3) == 0 ? true : false;
        }

        public void SaveWorld()
        {
            for (int i = 0; i < WorldMap.GetLength(0); i++)
                for (int j = 0; j < WorldMap.GetLength(1); j++)
                    NextGen[i, j] = WorldMap[i, j];
        }

        public void LoadWorld()
        {
            for (int i = 0; i < WorldMap.GetLength(0); i++)
                for (int j = 0; j < WorldMap.GetLength(1); j++)
                    WorldMap[i, j] = NextGen[i, j];
        }

        public void CalculateNextGen()
        {
            for (int i = 0; i < WorldMap.GetLength(0); i++)
                for (int j = 0; j < WorldMap.GetLength(1); j++)
                {
                    byte count = 0;
                    bool dirR = j + 1 < WorldMap.GetLength(1);
                    bool dirL = j - 1 >= 0;
                    bool dirD = i + 1 < WorldMap.GetLength(0);
                    bool dirU = i - 1 >= 0;

                    if (dirL && dirU)
                        if (WorldMap[i - 1, j - 1])
                            count++;
                    if (dirU)
                        if (WorldMap[i - 1, j])
                            count++;
                    if (dirR && dirU)
                        if (WorldMap[i - 1, j + 1])
                            count++;
                    if (dirL && dirD)
                        if (WorldMap[i + 1, j - 1])
                            count++;
                    if (dirD)
                        if (WorldMap[i + 1, j])
                            count++;
                    if (dirR && dirD)
                        if (WorldMap[i + 1, j + 1])
                            count++;
                    if (dirL)
                        if (WorldMap[i, j - 1])
                            count++;
                    if (dirR)
                        if (WorldMap[i, j + 1])
                            count++;

                    if ((!WorldMap[i, j]) && (count == 3))
                        NextGen[i, j] = true;

                    if ((WorldMap[i, j]) && !(count == 2 || count == 3))
                        NextGen[i, j] = false;
                }
        }

        public void Draw()
        {
            Console.SetCursorPosition(0, 0);
            for (int i = 0; i < WorldMap.GetLength(0); i++)
            {
                for (int j = 0; j < WorldMap.GetLength(1); j++)
                    Console.Write(WorldMap[i, j] ? "X" : " ");
                Console.Write("\n");
            }
        }
    }
}