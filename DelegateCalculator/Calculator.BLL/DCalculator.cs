﻿using Calculator.BLL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.BBL
{
    public class DCalculator : ICalculator
    {
        private Dictionary<string, Func<double, double, double>> _operations = new Dictionary<string, Func<double, double, double>>
        {
            ["+"] = (x, y) => x + y,
            ["-"] = (x, y) => x - y,
            ["*"] = (x, y) => x * y,
            ["/"] = (x, y) => x / y
        };

        public Dictionary<string, Func<double, double, double>> Operations { get { return _operations; } }

        public double Calculate(string op, double x, double y)
        {

            if ((op == "/") && (y == 0))
                throw new DivideByZeroException(string.Format($"Division by zero is not allowed."));
            return _operations[op](x, y);
        }
    }
}
