﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.BLL.Contracts
{
    public interface ICalculator
    {
        double Calculate(string op, double x, double y);
    }
}
