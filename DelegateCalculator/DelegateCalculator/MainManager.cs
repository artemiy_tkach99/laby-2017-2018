﻿using Calculator.BBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateCalculator
{
    class MainManager
    {
        private readonly DCalculator _calculator = new DCalculator();
        private readonly InputPromptManager _inputManager = new InputPromptManager();
        private double _valueX;
        private double _valueY;
        private string _op;

        private void InputData()
        {
            _valueX = _inputManager.InputNumberPrompt("\nEnter first operand: ");
            _op = _inputManager.InputOperatorPrompt("\nEnter the operator (+, -, *, /): ");
            _valueY = _inputManager.InputNumberPrompt("\nEnter second operand: ");
        }

        public void Start()
        {
            bool keepOn = true;
            while (keepOn)
            {
                Console.WriteLine("=== Calculator ===\n");
                InputData();
                try
                {
                    var result = _calculator.Calculate(_op, _valueX, _valueY);
                    Console.WriteLine($"\n{_valueX} {_op} {_valueY} = {result}");
                }
                catch (DivideByZeroException)
                {
                    Console.WriteLine("\nError. Can't divide by zero.");
                }
                keepOn = _inputManager.RestartPrompt("Perform another calculation? (y/n): ");
                Console.WriteLine("\n\n");
            }
        }
    }
}
