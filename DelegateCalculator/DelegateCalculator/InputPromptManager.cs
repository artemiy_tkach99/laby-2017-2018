﻿using Calculator.BBL;
using Calculator.BLL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateCalculator
{
    class InputPromptManager
    {
        private readonly DCalculator _calculator = new DCalculator();

        public double InputNumberPrompt(string promptText)
        {
            Console.Write(promptText);
            double tempInput;
            while (!Double.TryParse(Console.ReadLine(), out tempInput))
            {
                Console.WriteLine("Error. Input is not a numerical value. Try again. ");
                Console.Write(promptText);
            }
            return tempInput;
        }

        public string InputOperatorPrompt(string promptText)
        {
            Console.Write(promptText);
            string tempInput = Console.ReadLine();

            while (!_calculator.Operations.ContainsKey(tempInput))
            {
                Console.WriteLine("Error. Input is not a valid operation. Try again. ");
                Console.Write(promptText);
                tempInput = Console.ReadLine();
            }
            return tempInput;
        }

        public bool RestartPrompt(string promptText)
        {
            Console.Write(promptText);
            string tempInput = Console.ReadLine();
            while (tempInput != "y" && tempInput != "n" &&
                tempInput != "Y" && tempInput != "N")
            {
                Console.WriteLine("Error. Input is not a valid option. Try again. ");
                Console.Write(promptText);
                tempInput = Console.ReadLine();
            }

            if (tempInput == "y" || tempInput == "Y")
                return true;
            else
                return false;
        }
    }
}
