﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListRealization
{
    public class ListArgs<T>
    {
        public string Message { get; }
        public bool IsValidation { get; set; }
        public T Element { get; set; }
        public ListArgs(string message)
        {
            Message = message;
            IsValidation = true;
        }
        public ListArgs(string message, T item)
        {
            Message = message;
            Element = item;
            IsValidation = true;
        }
    }
}
