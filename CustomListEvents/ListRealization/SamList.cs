﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListRealization
{
    [Serializable]
    public class SamList<T> : Collection<T>
    {
        public SamList() : this(new List<T>()) { }
        public SamList(List<T> list) : base(list) { }
        public event EventHandler<ListArgs<T>> BeforeAdd;
        public event EventHandler<ListArgs<T>> AfterAdd;
        public event EventHandler<ListArgs<T>> BeforeRemove;
        public event EventHandler<ListArgs<T>> AfterRemove;
        public void OnBeforeAdded(ListArgs<T> e) { BeforeAdd?.Invoke(this, e); }
        public void OnAfterAdded(ListArgs<T> e) { AfterAdd?.Invoke(this, e); }
        public void OnBeforeRemoved(ListArgs<T> e) { BeforeRemove?.Invoke(this, e); }
        public void OnAfterRemoved(ListArgs<T> e) { AfterRemove?.Invoke(this, e); }
        protected override void InsertItem(int index, T item)
        {
            var Add = new ListArgs<T>("Element will be verify",item);
            OnBeforeAdded(Add);
            if (Add.IsValidation)
            {
                OnBeforeAdded(new ListArgs<T>($"Element {item} will be add", item));
                base.InsertItem(index, item);
            }
            else
            {
                OnAfterAdded(new ListArgs<T>($"Element {item} can't be added, check the rules of adding "));
            }
        }

        protected override void RemoveItem(int index)
        {

            var remove = new ListArgs<T>("Element will be verify");
            OnBeforeRemoved(remove);
            {
                if (remove.IsValidation)
                {
                    OnBeforeRemoved(new ListArgs<T>($"Element with index {index} will be remove"));
                    base.RemoveItem(index);
                    OnAfterRemoved(new ListArgs<T>($"Element with index {index} was removed"));
                }
                else
                {
                    Console.WriteLine("No such index");
                }
            }
        }
        public void Sort()
        {
            ((List<T>)Items).Sort();
        }
    }
}
